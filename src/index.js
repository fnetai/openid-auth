import { Issuer } from 'openid-client';

/**
 * Authenticates a user with an OpenID Connect provider using the Resource Owner Password Credentials Grant (Direct Access) and retrieves an access token.
 * Also provides a method to refresh the token or re-authenticate if the refresh token is expired.
 * Leverages the openid-client library for streamlined OAuth 2.0 and OpenID Connect interactions.
 * Adds a created_at timestamp to manage token expiration effectively.
 *
 * @param {object} args - The arguments object.
 * @param {string} args.issuer_url - The issuer URL of the OpenID Connect provider.
 * @param {string} args.grant_type - The grant type to use for authentication.
 * @param {string} args.client_id - The client ID configured in the provider.
 * @param {string} args.client_secret - The client secret configured in the provider.
 * @param {string} args.username - The username of the user.
 * @param {string} args.password - The password of the user.
 * @returns {Promise<object>} A promise that resolves with the token data and a refresh function.
 */
export default async ({
  issuer_url,
  grant_type,
  client_id,
  client_secret,
  username,
  password
}) => {
  try {
    grant_type = grant_type || 'password';

    const discoveredIssuer = await Issuer.discover(issuer_url);
    const client = new discoveredIssuer.Client({
      client_id,
      client_secret,
    });

    const grantOptions = {
      grant_type,
    }

    if (grant_type === 'password') {
      grantOptions.username = username;
      grantOptions.password = password;
    }
    else if (grant_type === 'client_credentials') {
      grantOptions.client_id = client_id;
      grantOptions.client_secret = client_secret;
    }
    else throw new Error(`Unsupported grant type: ${grant_type}`);

    const tokenSet = await client.grant(grantOptions);

    // Add a created_at field to the token object to track the creation time
    const enhancedTokenSet = {
      ...tokenSet,
      created_at: new Date().getTime(),
    };

    const refreshToken = async (force = false) => {
      const currentTime = new Date().getTime();
      // Check if it's time to refresh the token or force-refresh is requested
      if (force || currentTime >= enhancedTokenSet.expires_at * 1000) {
        const refreshedTokenSet = await client.refresh(enhancedTokenSet.refresh_token);
        // Update the created_at time for the new token
        return {
          ...refreshedTokenSet,
          created_at: new Date().getTime(),
        };
      }
      return enhancedTokenSet;
    };

    return {
      token: enhancedTokenSet,
      refreshToken,
    };
  } catch (error) {
    console.error('Error during authentication or token refresh:', error);
    throw error;
  }
};